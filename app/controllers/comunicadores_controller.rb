class ComunicadoresController < ApplicationController
  # GET /comunicadores
  # GET /comunicadores.json
  def index
    @comunicadores = Comunicadore.all
    respond_to do |format|
      format.html { render :layout => 'admin'}# index.html.erb
      format.json { render :json => @comunicadores }
    end
  end

  def comunicadores

    @upload_sec_banners = UploadSecBanner.all
    @upload_main_banners = UploadMainBanner.all  

    @comunicadores = Comunicadore.paginate(:page => params[:page], :per_page => 2)
    @contato = Contato.new
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @comunicadores }
    end
  end

  # GET /comunicadores/1
  # GET /comunicadores/1.json
  def mostrar
    @upload_sec_banners = UploadSecBanner.all
    @upload_main_banners = UploadMainBanner.all  

    @comunicadore = Comunicadore.find(params[:id])
    @contato = Contato.new
    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @comunicadore }
    end
  end


  # GET /comunicadores/1
  # GET /comunicadores/1.json
  def show
    @comunicadore = Comunicadore.find(params[:id])
    
    respond_to do |format|
      format.html { render :layout => 'admin'}# show.html.erb
      format.json { render :json => @comunicadore }
    end
  end

  # GET /comunicadores/new
  # GET /comunicadores/new.json
  def new
    @comunicadore = Comunicadore.new
    
    respond_to do |format|
      format.html { render :layout => 'admin'}# new.html.erb
      format.json { render :json => @comunicadore }
    end
  end

  # GET /comunicadores/1/edit
  def edit
    @comunicadore = Comunicadore.find(params[:id])    
     render :layout => 'admin'
  end

  # POST /comunicadores
  # POST /comunicadores.json
  def create
    @comunicadore = Comunicadore.new(params[:comunicadore])

    respond_to do |format|
      if @comunicadore.save
        format.html { redirect_to @comunicadore, :notice => 'Comunicadore was successfully created.' }
        format.json { render :json => @comunicadore, :status => :created, :location => @comunicadore }
      else
        format.html { render :action => "new" }
        format.json { render :json => @comunicadore.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /comunicadores/1
  # PUT /comunicadores/1.json
  def update
    @comunicadore = Comunicadore.find(params[:id])

    respond_to do |format|
      if @comunicadore.update_attributes(params[:comunicadore])
        format.html { redirect_to @comunicadore, :notice => 'Comunicadore was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @comunicadore.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /comunicadores/1
  # DELETE /comunicadores/1.json
  def destroy
    @comunicadore = Comunicadore.find(params[:id])
    @comunicadore.destroy

    respond_to do |format|
      format.html { redirect_to comunicadores_url }
      format.json { head :no_content }
    end
  end
end
