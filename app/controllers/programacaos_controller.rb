class ProgramacaosController < ApplicationController
  http_basic_authenticate_with :name => "radio", :password => "radio", :except => :programacao
  # GET /programacao
  # GET /programacao.json
  def programacao

    @upload_sec_banners = UploadSecBanner.all
    @upload_main_banners = UploadMainBanner.all  

    @contato = Contato.new
    @dias = Dia.find(params[:dia])
    @programacaos = @dias.programacaos.paginate(:page => params[:page], :per_page => 2)
    respond_to do |format|
      format.html
      format.json { render :json => @programacaos }
    end
  end
  

  # GET /programacaos
  # GET /programacaos.json
  def index
    @programacaos = Programacao.all

    respond_to do |format|
      format.html { render :layout => 'admin'}
      format.json { render :json => @programacaos }
    end
  end

  # GET /programacao/1
  # GET /programacao/1.json
  def mostrar
    @upload_sec_banners = UploadSecBanner.all
    @upload_main_banners = UploadMainBanner.all  

    @programacao = Programacao.find(params[:id])
    @contato = Contato.new
    respond_to do |format|
      format.html 
      format.json { render :json => @programacao }
    end
  end


  # GET /programacaos/1
  # GET /programacaos/1.json
  def show
    @programacao = Programacao.find(params[:id])
    
    respond_to do |format|
      format.html { render :layout => 'admin'}# show.html.erb
      format.json { render :json => @programacao }
    end
  end

  # GET /programacaos/new
  # GET /programacaos/new.json
  def new
    @programacao = Programacao.new
    
    respond_to do |format|  
      format.html { render :layout => 'admin'}# new.html.erb
      format.json { render :json => @programacao }
    end
  end

  # GET /programacaos/1/edit
  def edit
    @programacao = Programacao.find(params[:id])
    render :layout => 'admin'
  end

  # POST /programacaos
  # POST /programacaos.json
  def create
    @programacao = Programacao.new(params[:programacao])

    respond_to do |format|
      if @programacao.save
        format.html { redirect_to @programacao, :notice => 'Programacao was successfully created.' }
        format.json { render :json => @programacao, :status => :created, :location => @programacao }
      else
        format.html { render :action => "new" }
        format.json { render :json => @programacao.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /programacaos/1
  # PUT /programacaos/1.json
  def update
    @programacao = Programacao.find(params[:id])

    respond_to do |format|
      if @programacao.update_attributes(params[:programacao])
        format.html { redirect_to @programacao, :notice => 'Programacao was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @programacao.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /programacaos/1
  # DELETE /programacaos/1.json
  def destroy
    @programacao = Programacao.find(params[:id])
    @programacao.destroy

    respond_to do |format|
      format.html { redirect_to programacaos_url }
      format.json { head :no_content }
    end
  end
end
