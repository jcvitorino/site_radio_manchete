class RecadoOuvintesController < ApplicationController
  # GET /recado_ouvintes
  # GET /recado_ouvintes.json
  def index
    @recado_ouvintes = RecadoOuvinte.all
    
    respond_to do |format|
      format.html { render :layout => 'admin'}# index.html.erb
      format.json { render :json => @recado_ouvintes }
    end
  end

  # GET /recado_ouvintes/1
  # GET /recado_ouvintes/1.json
  def show
    @upload_sec_banners = UploadSecBanner.all
    @upload_main_banners = UploadMainBanner.all  

    @recado_ouvinte = RecadoOuvinte.find(params[:id])
    @contato = Contato.new
    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @recado_ouvinte }
    end
  end

  # GET /recado_ouvintes/new
  # GET /recado_ouvintes/new.json
  def new
    @upload_sec_banners = UploadSecBanner.all
    @upload_main_banners = UploadMainBanner.all  

    @recado_ouvintes = RecadoOuvinte.find_by_sql("select * from recado_ouvintes ORDER BY id  DESC LIMIT 10")
    @recado_ouvinte = RecadoOuvinte.new
    @contato = Contato.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @recado_ouvinte }
    end
  end

  # GET /recado_ouvintes/1/edit
  def edit
    @upload_sec_banners = UploadSecBanner.all
    @upload_main_banners = UploadMainBanner.all  

    @recado_ouvinte = RecadoOuvinte.find(params[:id])
    @contato = Contato.new
  end

  # POST /recado_ouvintes
  # POST /recado_ouvintes.json
  def create
    @recado_ouvinte = RecadoOuvinte.new(params[:recado_ouvinte])
  
    respond_to do |format|
      if @recado_ouvinte.save
        format.html { redirect_to @recado_ouvinte, :notice => 'Recado ouvinte was successfully created.' }
        format.json { render :json => @recado_ouvinte, :status => :created, :location => @recado_ouvinte }
      else
        format.html { render :action => "new" }
        format.json { render :json => @recado_ouvinte.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /recado_ouvintes/1
  # PUT /recado_ouvintes/1.json
  def update
    @recado_ouvinte = RecadoOuvinte.find(params[:id])

    respond_to do |format|
      if @recado_ouvinte.update_attributes(params[:recado_ouvinte])
        format.html { redirect_to @recado_ouvinte, :notice => 'Recado ouvinte was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @recado_ouvinte.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /recado_ouvintes/1
  # DELETE /recado_ouvintes/1.json
  def destroy
    @recado_ouvinte = RecadoOuvinte.find(params[:id])
    @recado_ouvinte.destroy

    respond_to do |format|
      format.html { redirect_to recado_ouvintes_url }
      format.json { head :no_content }
    end
  end
end
