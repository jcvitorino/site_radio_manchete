class StaticPagesController < ApplicationController
  def home
    @upload_sec_banners = UploadSecBanner.all
  	@upload_main_banners = UploadMainBanner.all  
  	@posts = Post.find_by_sql("select * from posts ORDER BY id  DESC LIMIT 6")
  	@recado_ouvinte =  RecadoOuvinte.new  	
  	@contato = Contato.new
    @prog_atual = Programacao.find_by_sql("SELECT * from programacaos where time(hora_inicio) < \"#{Time.now.strftime("%H:%M:%S")}\" limit 1").first
    @prog_prox = Programacao.find_by_sql("SELECT * from programacaos where time(hora_inicio) < \"#{Time.now.strftime("%H:%M:%S")}\" limit 1").first
  end

  def radio
    @upload_sec_banners = UploadSecBanner.all
  	@upload_main_banners = UploadMainBanner.all
  	@contato = Contato.new
  end
end
