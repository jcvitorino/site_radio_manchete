class UploadMainBannersController < ApplicationController
  # GET /upload_main_banners
  # GET /upload_main_banners.json
  def index
    @upload_main_banners = UploadMainBanner.all

    respond_to do |format|
      format.html { render :layout => 'admin'}# index.html.erb
      format.json { render :json => @upload_main_banners }
    end
  end

  # GET /upload_main_banners/1
  # GET /upload_main_banners/1.json
  def show
    @upload_main_banner = UploadMainBanner.find(params[:id])

    respond_to do |format|
      format.html { render :layout => 'admin'}# show.html.erb
      format.json { render :json => @upload_main_banner }
    end
  end

  # GET /upload_main_banners/new
  # GET /upload_main_banners/new.json
  def new
    @upload_main_banner = UploadMainBanner.new

    respond_to do |format|
      format.html { render :layout => 'admin'}# new.html.erb
      format.json { render :json => @upload_main_banner }
    end
  end

  # GET /upload_main_banners/1/edit
  def edit
    @upload_main_banner = UploadMainBanner.find(params[:id])
  end

  # POST /upload_main_banners
  # POST /upload_main_banners.json
  def create
    @upload_main_banner = UploadMainBanner.new(params[:upload_main_banner])

    respond_to do |format|
      if @upload_main_banner.save
        format.html { redirect_to @upload_main_banner, :notice => 'Upload main banner was successfully created.' }
        format.json { render :json => @upload_main_banner, :status => :created, :location => @upload_main_banner }
      else
        format.html { render :action => "new" }
        format.json { render :json => @upload_main_banner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /upload_main_banners/1
  # PUT /upload_main_banners/1.json
  def update
    @upload_main_banner = UploadMainBanner.find(params[:id])

    respond_to do |format|
      if @upload_main_banner.update_attributes(params[:upload_main_banner])
        format.html { redirect_to @upload_main_banner, :notice => 'Upload main banner was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @upload_main_banner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /upload_main_banners/1
  # DELETE /upload_main_banners/1.json
  def destroy
    @upload_main_banner = UploadMainBanner.find(params[:id])
    @upload_main_banner.destroy

    respond_to do |format|
      format.html { redirect_to upload_main_banners_url }
      format.json { head :no_content }
    end
  end
end
