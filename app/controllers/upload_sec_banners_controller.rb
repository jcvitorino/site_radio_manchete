class UploadSecBannersController < ApplicationController
  # GET /upload_sec_banners
  # GET /upload_sec_banners.json
  def index
    @upload_sec_banners = UploadSecBanner.all

    respond_to do |format|
      format.html { render :layout => 'admin'}# index.html.erb
      format.json { render :json => @upload_sec_banners }
    end
  end

  # GET /upload_sec_banners/1
  # GET /upload_sec_banners/1.json
  def show
    @upload_sec_banner = UploadSecBanner.find(params[:id])

    respond_to do |format|
      format.html { render :layout => 'admin'}# show.html.erb
      format.json { render :json => @upload_sec_banner }
    end
  end

  # GET /upload_sec_banners/new
  # GET /upload_sec_banners/new.json
  def new
    @upload_sec_banner = UploadSecBanner.new

    respond_to do |format|
      format.html { render :layout => 'admin'}# new.html.erb
      format.json { render :json => @upload_sec_banner }
    end
  end

  # GET /upload_sec_banners/1/edit
  def edit
    @upload_sec_banner = UploadSecBanner.find(params[:id])
  end

  # POST /upload_sec_banners
  # POST /upload_sec_banners.json
  def create
    @upload_sec_banner = UploadSecBanner.new(params[:upload_sec_banner])

    respond_to do |format|
      if @upload_sec_banner.save
        format.html { redirect_to @upload_sec_banner, :notice => 'Upload sec banner was successfully created.' }
        format.json { render :json => @upload_sec_banner, :status => :created, :location => @upload_sec_banner }
      else
        format.html { render :action => "new" }
        format.json { render :json => @upload_sec_banner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /upload_sec_banners/1
  # PUT /upload_sec_banners/1.json
  def update
    @upload_sec_banner = UploadSecBanner.find(params[:id])

    respond_to do |format|
      if @upload_sec_banner.update_attributes(params[:upload_sec_banner])
        format.html { redirect_to @upload_sec_banner, :notice => 'Upload sec banner was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @upload_sec_banner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /upload_sec_banners/1
  # DELETE /upload_sec_banners/1.json
  def destroy
    @upload_sec_banner = UploadSecBanner.find(params[:id])
    @upload_sec_banner.destroy

    respond_to do |format|
      format.html { redirect_to upload_sec_banners_url }
      format.json { head :no_content }
    end
  end
end
