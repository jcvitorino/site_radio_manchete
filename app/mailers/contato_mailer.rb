class ContatoMailer < ActionMailer::Base
  default from: "josue.crispim@gmail.com"

  def mensagem(contato,mensagem,assunto)
    @contato = contato    

    mail(:to => "josue.crispim@gmail.com", :subject => assunto)
  end
end