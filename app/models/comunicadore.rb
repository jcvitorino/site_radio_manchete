class Comunicadore < ActiveRecord::Base
  attr_accessible :contato, :nome, :sobre, :foto
  mount_uploader :foto, NoticiaUploader
end
