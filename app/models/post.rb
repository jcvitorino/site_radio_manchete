# == Schema Information
#
# Table name: posts
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'carrierwave/orm/activerecord'

class Post < ActiveRecord::Base
  attr_accessible :description, :title, :manchete, :imagem
  mount_uploader :imagem, NoticiaUploader
  self.per_page = 3
end
