class Programacao < ActiveRecord::Base
  attr_accessible :comunicador_id, :descricao, :dia_id, :hora_inicio, :hora_fim, :nome
  belongs_to :comunicadore, :class_name => Comunicadore, :foreign_key => :comunicador_id
  belongs_to :dia
end
