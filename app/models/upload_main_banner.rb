class UploadMainBanner < ActiveRecord::Base
  attr_accessible :mainbanner
  mount_uploader :mainbanner, MainbannerUploader
end
