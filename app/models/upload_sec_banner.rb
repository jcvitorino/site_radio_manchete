class UploadSecBanner < ActiveRecord::Base
  attr_accessible :secbanner1, :secbanner2, :secbanner3
  mount_uploader :secbanner1, Secbanner1Uploader
  mount_uploader :secbanner2, Secbanner2Uploader
  mount_uploader :secbanner3, Secbanner3Uploader
end
