class CreateRecadoOuvintes < ActiveRecord::Migration
  def change
    create_table :recado_ouvintes do |t|
      t.string :nome
      t.string :telefone
      t.string :email
      t.text :recado

      t.timestamps
    end
  end
end
