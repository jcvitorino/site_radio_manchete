class AddImagemToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :imagem, :string
    add_column :posts, :manchete, :string
  end
end
