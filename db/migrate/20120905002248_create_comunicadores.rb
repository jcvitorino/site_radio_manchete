class CreateComunicadores < ActiveRecord::Migration
  def change
    create_table :comunicadores do |t|
      t.string :nome
      t.text :sobre
      t.string :contato

      t.timestamps
    end
  end
end
