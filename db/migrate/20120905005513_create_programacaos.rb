class CreateProgramacaos < ActiveRecord::Migration
  def change
    create_table :programacaos do |t|
      t.string :nome
      t.integer :comunicador_id
      t.integer :dia_id
      t.string :hora_inicio
      t.string :hora_fim
      t.text :descricao

      t.timestamps
    end
  end
end
