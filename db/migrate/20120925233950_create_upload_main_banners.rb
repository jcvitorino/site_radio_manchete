class CreateUploadMainBanners < ActiveRecord::Migration
  def change
    create_table :upload_main_banners do |t|
      t.string :mainbanner

      t.timestamps
    end
  end
end
