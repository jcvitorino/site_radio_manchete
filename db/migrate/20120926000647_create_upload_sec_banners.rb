class CreateUploadSecBanners < ActiveRecord::Migration
  def change
    create_table :upload_sec_banners do |t|
      t.string :secbanner1
      t.string :secbanner2
      t.string :secbanner3

      t.timestamps
    end
  end
end
