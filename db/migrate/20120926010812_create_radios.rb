class CreateRadios < ActiveRecord::Migration
  def change
    create_table :radios do |t|
      t.text :descricao

      t.timestamps
    end
  end
end
