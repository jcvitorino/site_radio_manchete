class ChangeProgramacaoes < ActiveRecord::Migration
  def up
  	change_column :programacaos, :hora_inicio, :time
  end

  def down
  	change_column :programacaos, :hora_inicio, :string 
  end
end
