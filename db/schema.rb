# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121003014731) do

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "comunicadores", :force => true do |t|
    t.string   "nome"
    t.text     "sobre"
    t.string   "contato"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "foto"
  end

  create_table "contatos", :force => true do |t|
    t.string   "nome"
    t.string   "email"
    t.string   "assunto"
    t.text     "conteudo"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "dia", :force => true do |t|
    t.string   "nome"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "imagem"
    t.string   "manchete"
  end

  create_table "programacaos", :force => true do |t|
    t.string   "nome"
    t.integer  "comunicador_id"
    t.integer  "dia_id"
    t.time     "hora_inicio"
    t.string   "hora_fim"
    t.text     "descricao"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "radios", :force => true do |t|
    t.text     "descricao"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "recado_ouvintes", :force => true do |t|
    t.string   "nome"
    t.string   "telefone"
    t.string   "email"
    t.text     "recado"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "upload_main_banners", :force => true do |t|
    t.string   "mainbanner"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "upload_sec_banners", :force => true do |t|
    t.string   "secbanner1"
    t.string   "secbanner2"
    t.string   "secbanner3"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "password_digest"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

end
