require 'spec_helper'

describe "StaticPages" do
  subject { page }  
  describe "Home page" do
  	before { visit root_path }  	
    it { should have_selector('div', text: "Aqui vai o banner principal") }
  end
  
  describe "Radio page" do
  	before { visit radio_path }  	
    it { should have_selector('p', text: "Pagina sobre a radio") }
  end
end
