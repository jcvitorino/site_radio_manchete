require "spec_helper"

describe ComunicadoresController do
  describe "routing" do

    it "routes to #index" do
      get("/comunicadores").should route_to("comunicadores#index")
    end

    it "routes to #new" do
      get("/comunicadores/new").should route_to("comunicadores#new")
    end

    it "routes to #show" do
      get("/comunicadores/1").should route_to("comunicadores#show", :id => "1")
    end

    it "routes to #edit" do
      get("/comunicadores/1/edit").should route_to("comunicadores#edit", :id => "1")
    end

    it "routes to #create" do
      post("/comunicadores").should route_to("comunicadores#create")
    end

    it "routes to #update" do
      put("/comunicadores/1").should route_to("comunicadores#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/comunicadores/1").should route_to("comunicadores#destroy", :id => "1")
    end

  end
end
