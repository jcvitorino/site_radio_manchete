require "spec_helper"

describe ProgramacaosController do
  describe "routing" do

    it "routes to #index" do
      get("/programacaos").should route_to("programacaos#index")
    end

    it "routes to #new" do
      get("/programacaos/new").should route_to("programacaos#new")
    end

    it "routes to #show" do
      get("/programacaos/1").should route_to("programacaos#show", :id => "1")
    end

    it "routes to #edit" do
      get("/programacaos/1/edit").should route_to("programacaos#edit", :id => "1")
    end

    it "routes to #create" do
      post("/programacaos").should route_to("programacaos#create")
    end

    it "routes to #update" do
      put("/programacaos/1").should route_to("programacaos#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/programacaos/1").should route_to("programacaos#destroy", :id => "1")
    end

  end
end
