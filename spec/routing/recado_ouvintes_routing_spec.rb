require "spec_helper"

describe RecadoOuvintesController do
  describe "routing" do

    it "routes to #index" do
      get("/recado_ouvintes").should route_to("recado_ouvintes#index")
    end

    it "routes to #new" do
      get("/recado_ouvintes/new").should route_to("recado_ouvintes#new")
    end

    it "routes to #show" do
      get("/recado_ouvintes/1").should route_to("recado_ouvintes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/recado_ouvintes/1/edit").should route_to("recado_ouvintes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/recado_ouvintes").should route_to("recado_ouvintes#create")
    end

    it "routes to #update" do
      put("/recado_ouvintes/1").should route_to("recado_ouvintes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/recado_ouvintes/1").should route_to("recado_ouvintes#destroy", :id => "1")
    end

  end
end
