require "spec_helper"

describe UploadSecBannersController do
  describe "routing" do

    it "routes to #index" do
      get("/upload_sec_banners").should route_to("upload_sec_banners#index")
    end

    it "routes to #new" do
      get("/upload_sec_banners/new").should route_to("upload_sec_banners#new")
    end

    it "routes to #show" do
      get("/upload_sec_banners/1").should route_to("upload_sec_banners#show", :id => "1")
    end

    it "routes to #edit" do
      get("/upload_sec_banners/1/edit").should route_to("upload_sec_banners#edit", :id => "1")
    end

    it "routes to #create" do
      post("/upload_sec_banners").should route_to("upload_sec_banners#create")
    end

    it "routes to #update" do
      put("/upload_sec_banners/1").should route_to("upload_sec_banners#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/upload_sec_banners/1").should route_to("upload_sec_banners#destroy", :id => "1")
    end

  end
end
