require 'spec_helper'

describe "comunicadores/edit" do
  before(:each) do
    @comunicadore = assign(:comunicadore, stub_model(Comunicadore,
      :nome => "MyString",
      :sobre => "MyText",
      :contato => "MyString"
    ))
  end

  it "renders the edit comunicadore form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => comunicadores_path(@comunicadore), :method => "post" do
      assert_select "input#comunicadore_nome", :name => "comunicadore[nome]"
      assert_select "textarea#comunicadore_sobre", :name => "comunicadore[sobre]"
      assert_select "input#comunicadore_contato", :name => "comunicadore[contato]"
    end
  end
end
