require 'spec_helper'

describe "comunicadores/index" do
  before(:each) do
    assign(:comunicadores, [
      stub_model(Comunicadore,
        :nome => "Nome",
        :sobre => "MyText",
        :contato => "Contato"
      ),
      stub_model(Comunicadore,
        :nome => "Nome",
        :sobre => "MyText",
        :contato => "Contato"
      )
    ])
  end

  it "renders a list of comunicadores" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nome".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Contato".to_s, :count => 2
  end
end
