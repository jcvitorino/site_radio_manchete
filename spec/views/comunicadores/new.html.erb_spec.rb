require 'spec_helper'

describe "comunicadores/new" do
  before(:each) do
    assign(:comunicadore, stub_model(Comunicadore,
      :nome => "MyString",
      :sobre => "MyText",
      :contato => "MyString"
    ).as_new_record)
  end

  it "renders new comunicadore form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => comunicadores_path, :method => "post" do
      assert_select "input#comunicadore_nome", :name => "comunicadore[nome]"
      assert_select "textarea#comunicadore_sobre", :name => "comunicadore[sobre]"
      assert_select "input#comunicadore_contato", :name => "comunicadore[contato]"
    end
  end
end
