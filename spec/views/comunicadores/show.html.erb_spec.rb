require 'spec_helper'

describe "comunicadores/show" do
  before(:each) do
    @comunicadore = assign(:comunicadore, stub_model(Comunicadore,
      :nome => "Nome",
      :sobre => "MyText",
      :contato => "Contato"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nome/)
    rendered.should match(/MyText/)
    rendered.should match(/Contato/)
  end
end
