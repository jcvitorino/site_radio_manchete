require 'spec_helper'

describe "programacaos/index" do
  before(:each) do
    assign(:programacaos, [
      stub_model(Programacao,
        :nome => "Nome",
        :comunicador_id => 1,
        :dia_id => 2,
        :hora_id => 3,
        :descricao => "MyText"
      ),
      stub_model(Programacao,
        :nome => "Nome",
        :comunicador_id => 1,
        :dia_id => 2,
        :hora_id => 3,
        :descricao => "MyText"
      )
    ])
  end

  it "renders a list of programacaos" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nome".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
