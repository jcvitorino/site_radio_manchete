require 'spec_helper'

describe "programacaos/new" do
  before(:each) do
    assign(:programacao, stub_model(Programacao,
      :nome => "MyString",
      :comunicador_id => 1,
      :dia_id => 1,
      :hora_id => 1,
      :descricao => "MyText"
    ).as_new_record)
  end

  it "renders new programacao form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => programacaos_path, :method => "post" do
      assert_select "input#programacao_nome", :name => "programacao[nome]"
      assert_select "input#programacao_comunicador_id", :name => "programacao[comunicador_id]"
      assert_select "input#programacao_dia_id", :name => "programacao[dia_id]"
      assert_select "input#programacao_hora_id", :name => "programacao[hora_id]"
      assert_select "textarea#programacao_descricao", :name => "programacao[descricao]"
    end
  end
end
