require 'spec_helper'

describe "programacaos/show" do
  before(:each) do
    @programacao = assign(:programacao, stub_model(Programacao,
      :nome => "Nome",
      :comunicador_id => 1,
      :dia_id => 2,
      :hora_id => 3,
      :descricao => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nome/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/MyText/)
  end
end
