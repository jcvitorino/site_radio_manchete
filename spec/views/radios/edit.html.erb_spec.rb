require 'spec_helper'

describe "radios/edit" do
  before(:each) do
    @radio = assign(:radio, stub_model(Radio,
      :descricao => ""
    ))
  end

  it "renders the edit radio form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => radios_path(@radio), :method => "post" do
      assert_select "input#radio_descricao", :name => "radio[descricao]"
    end
  end
end
