require 'spec_helper'

describe "radios/index" do
  before(:each) do
    assign(:radios, [
      stub_model(Radio,
        :descricao => ""
      ),
      stub_model(Radio,
        :descricao => ""
      )
    ])
  end

  it "renders a list of radios" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
