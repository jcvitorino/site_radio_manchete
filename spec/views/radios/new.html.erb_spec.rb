require 'spec_helper'

describe "radios/new" do
  before(:each) do
    assign(:radio, stub_model(Radio,
      :descricao => ""
    ).as_new_record)
  end

  it "renders new radio form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => radios_path, :method => "post" do
      assert_select "input#radio_descricao", :name => "radio[descricao]"
    end
  end
end
