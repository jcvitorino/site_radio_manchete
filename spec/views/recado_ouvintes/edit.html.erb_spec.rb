require 'spec_helper'

describe "recado_ouvintes/edit" do
  before(:each) do
    @recado_ouvinte = assign(:recado_ouvinte, stub_model(RecadoOuvinte,
      :nome => "MyString",
      :telefone => "MyString",
      :email => "MyString",
      :recado => "MyText"
    ))
  end

  it "renders the edit recado_ouvinte form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => recado_ouvintes_path(@recado_ouvinte), :method => "post" do
      assert_select "input#recado_ouvinte_nome", :name => "recado_ouvinte[nome]"
      assert_select "input#recado_ouvinte_telefone", :name => "recado_ouvinte[telefone]"
      assert_select "input#recado_ouvinte_email", :name => "recado_ouvinte[email]"
      assert_select "textarea#recado_ouvinte_recado", :name => "recado_ouvinte[recado]"
    end
  end
end
