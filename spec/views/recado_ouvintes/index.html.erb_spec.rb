require 'spec_helper'

describe "recado_ouvintes/index" do
  before(:each) do
    assign(:recado_ouvintes, [
      stub_model(RecadoOuvinte,
        :nome => "Nome",
        :telefone => "Telefone",
        :email => "Email",
        :recado => "MyText"
      ),
      stub_model(RecadoOuvinte,
        :nome => "Nome",
        :telefone => "Telefone",
        :email => "Email",
        :recado => "MyText"
      )
    ])
  end

  it "renders a list of recado_ouvintes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Nome".to_s, :count => 2
    assert_select "tr>td", :text => "Telefone".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
