require 'spec_helper'

describe "recado_ouvintes/new" do
  before(:each) do
    assign(:recado_ouvinte, stub_model(RecadoOuvinte,
      :nome => "MyString",
      :telefone => "MyString",
      :email => "MyString",
      :recado => "MyText"
    ).as_new_record)
  end

  it "renders new recado_ouvinte form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => recado_ouvintes_path, :method => "post" do
      assert_select "input#recado_ouvinte_nome", :name => "recado_ouvinte[nome]"
      assert_select "input#recado_ouvinte_telefone", :name => "recado_ouvinte[telefone]"
      assert_select "input#recado_ouvinte_email", :name => "recado_ouvinte[email]"
      assert_select "textarea#recado_ouvinte_recado", :name => "recado_ouvinte[recado]"
    end
  end
end
