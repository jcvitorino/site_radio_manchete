require 'spec_helper'

describe "recado_ouvintes/show" do
  before(:each) do
    @recado_ouvinte = assign(:recado_ouvinte, stub_model(RecadoOuvinte,
      :nome => "Nome",
      :telefone => "Telefone",
      :email => "Email",
      :recado => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Nome/)
    rendered.should match(/Telefone/)
    rendered.should match(/Email/)
    rendered.should match(/MyText/)
  end
end
