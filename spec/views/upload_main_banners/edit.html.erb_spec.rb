require 'spec_helper'

describe "upload_main_banners/edit" do
  before(:each) do
    @upload_main_banner = assign(:upload_main_banner, stub_model(UploadMainBanner,
      :mainbanner => "MyString"
    ))
  end

  it "renders the edit upload_main_banner form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => upload_main_banners_path(@upload_main_banner), :method => "post" do
      assert_select "input#upload_main_banner_mainbanner", :name => "upload_main_banner[mainbanner]"
    end
  end
end
