require 'spec_helper'

describe "upload_main_banners/index" do
  before(:each) do
    assign(:upload_main_banners, [
      stub_model(UploadMainBanner,
        :mainbanner => "Mainbanner"
      ),
      stub_model(UploadMainBanner,
        :mainbanner => "Mainbanner"
      )
    ])
  end

  it "renders a list of upload_main_banners" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Mainbanner".to_s, :count => 2
  end
end
