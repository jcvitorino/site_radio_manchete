require 'spec_helper'

describe "upload_main_banners/new" do
  before(:each) do
    assign(:upload_main_banner, stub_model(UploadMainBanner,
      :mainbanner => "MyString"
    ).as_new_record)
  end

  it "renders new upload_main_banner form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => upload_main_banners_path, :method => "post" do
      assert_select "input#upload_main_banner_mainbanner", :name => "upload_main_banner[mainbanner]"
    end
  end
end
