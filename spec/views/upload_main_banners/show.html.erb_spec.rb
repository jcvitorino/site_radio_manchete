require 'spec_helper'

describe "upload_main_banners/show" do
  before(:each) do
    @upload_main_banner = assign(:upload_main_banner, stub_model(UploadMainBanner,
      :mainbanner => "Mainbanner"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Mainbanner/)
  end
end
