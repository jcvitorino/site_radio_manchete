require 'spec_helper'

describe "upload_sec_banners/index" do
  before(:each) do
    assign(:upload_sec_banners, [
      stub_model(UploadSecBanner,
        :secbanner1 => "Secbanner1",
        :secbanner2 => "Secbanner2",
        :secbanner3 => "Secbanner3"
      ),
      stub_model(UploadSecBanner,
        :secbanner1 => "Secbanner1",
        :secbanner2 => "Secbanner2",
        :secbanner3 => "Secbanner3"
      )
    ])
  end

  it "renders a list of upload_sec_banners" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Secbanner1".to_s, :count => 2
    assert_select "tr>td", :text => "Secbanner2".to_s, :count => 2
    assert_select "tr>td", :text => "Secbanner3".to_s, :count => 2
  end
end
