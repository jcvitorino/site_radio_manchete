require 'spec_helper'

describe "upload_sec_banners/new" do
  before(:each) do
    assign(:upload_sec_banner, stub_model(UploadSecBanner,
      :secbanner1 => "MyString",
      :secbanner2 => "MyString",
      :secbanner3 => "MyString"
    ).as_new_record)
  end

  it "renders new upload_sec_banner form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => upload_sec_banners_path, :method => "post" do
      assert_select "input#upload_sec_banner_secbanner1", :name => "upload_sec_banner[secbanner1]"
      assert_select "input#upload_sec_banner_secbanner2", :name => "upload_sec_banner[secbanner2]"
      assert_select "input#upload_sec_banner_secbanner3", :name => "upload_sec_banner[secbanner3]"
    end
  end
end
