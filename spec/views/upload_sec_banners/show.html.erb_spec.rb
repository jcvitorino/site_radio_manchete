require 'spec_helper'

describe "upload_sec_banners/show" do
  before(:each) do
    @upload_sec_banner = assign(:upload_sec_banner, stub_model(UploadSecBanner,
      :secbanner1 => "Secbanner1",
      :secbanner2 => "Secbanner2",
      :secbanner3 => "Secbanner3"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Secbanner1/)
    rendered.should match(/Secbanner2/)
    rendered.should match(/Secbanner3/)
  end
end
